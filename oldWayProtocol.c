#include "key.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>       /* time */
INPUT __initKey__();
void move(INPUT input,WORD command);
void releaseKey(INPUT input,WORD command);

int main(int argc, char const *argv[]) {
	INPUT keyboard;
	keyboard = __initKey__();
	int condition = TRUE;
	WORD key = 0x4C;
	int i = 0;
	time_t start, end;
	//This is test only, too lazy to isolate this
	time_t t;
  	int times[5] = {0,0,0,0,0};
   	/* Intializes random number generator */
  	srand((unsigned) time(&t));
  	system("cls");
  	printf("Imagine aquilo que for pedido e pare apenas quando a nova instrucao for pedida\n");
  	printf("Cada cena imaginada deve durar, ao menos 5 segundos, se preferir, repita a cena mentalmente\n");
  	system("pause");
  	system("cls");
	while(condition){
		int number = rand()%10;
		switch(number){
			case 0:
			case 1:
				printf("Quando a contagem acabar\nImagine uma bolinha caindo de uma escada, degrau a degrau\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");
				Sleep(800);
				move(keyboard,keys[0]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				printf("Quando a contagem acabar\nImagine um golfinho pulando agua a fora\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");
				Sleep(800);
				releaseKey(keyboard,keys[0]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				++times[0];
				break;
			case 2:
			case 3:
				printf("Quando a contagem acabar\nImagine um copo com agua mudando de cor\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");
				Sleep(800);
				move(keyboard,keys[1]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				printf("Quando a contagem acabar\nImagine um disquete sendo martelado\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");
				Sleep(800);
				releaseKey(keyboard,keys[1]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				++times[1];
				break;
			case 4:
			case 5:
				printf("Quando a contagem acabar\nImagine uma fita cassete sendo ejetada\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");
				Sleep(800);
				move(keyboard,keys[2]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				printf("Quando a contagem acabar\nImagine uma impressora cuspindo papeis completamente pretos\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");
				Sleep(800);
				releaseKey(keyboard,keys[2]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				++times[2];
				break;
			case 6:
			case 7:
				printf("Quando a contagem acabar\nImagine um CD sendo guardado em sua caixa\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");
				Sleep(800);
				move(keyboard,keys[3]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				printf("Quando a contagem acabar\nImagine alguem jogando um balao de agua de um lugar muito alto\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");
				Sleep(800);
				releaseKey(keyboard,keys[3]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				++times[3];
				break;
			case 8:
			case 9:
				printf("Quando a contagem acabar\nImagine um papel sendo grampeado em uma parede\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");
				Sleep(800);
				move(keyboard,keys[4]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				printf("Quando a contagem acabar\nImagine um relogio cucco sendo ativado\n");
				Sleep(5000);
				printf("Imagine em 3...\n");
				Sleep(1000);
				printf("Imagine em 2...\n");
				Sleep(1000);
				printf("Imagine em 1...\n");
				Sleep(1000);
				printf("Agora.\n");printf("D\n");
				Sleep(800);
				releaseKey(keyboard,keys[4]);
				time(&start);
				do time(&end); while(difftime(end, start) <= 5);
				system("cls");
				++times[4];
				break;
		}
		/*
		printf("sleep for 1 sec\n");
		Sleep(1000);
		move(keyboard,a);
		Sleep(1000);
		releaseKey(keyboard,a);
		if(i > 10){
			condition = FALSE;
		}
		i++;*/
		//This is to make sure at least 10 times the scene will be imagined
		if (times[0] > 10 && times[2] > 10 && times[2] > 10 && times[3] > 10 && times[4] > 10){
			
			move(keyboard,key);
			break;
		}
	}
	printf("cena 1 e 2:%d\ncena 3 e 4:%d\ncena 5 e 6:%d\ncena 7 e 8:%d\ncena 9 e 10:%d\n",times[0],times[1],times[2],times[3],times[4]);
	releaseKey(keyboard,key);
	return 0;
}


INPUT __initKey__() {
	// This part needs to be moved
	INPUT input; // If I decide to go C++ is in = new INPUT();
	memset(&input,0,sizeof(input));
	input.type = INPUT_KEYBOARD;
	return input;
}

void move(INPUT input,WORD command) {
	input.ki.wVk = command;
	SendInput(1,&input,sizeof(input));
}

void releaseKey(INPUT input,WORD command){
	input.ki.wVk = command;
	input.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1,&input,sizeof(input));
}