//************************************************************************************************
//*Código adaptado de:                                                                          **
//* Milton Roberto Heinen                                                                       **
//* Simulador de redes neurais com o algoritmo backpropagation padrao (sem momentum)            **
//************************************************************************************************

#include "Neural.h"
int initializeAnn(const char *weightsFile){
    char line[LINE_SIZE_LIMIT + 1];
    char *szPalavra = NULL;
    FILE* fp = NULL;
    int i, j;
    // Salva os pesos
    if ((fp = fopen(weightsFile, "r")) == NULL)
      return 0;

    fgets(line, LINE_SIZE_LIMIT, fp);
    inputs = atoi(strtok(line, " "));
    hidden = atoi(strtok('\0', " "));
    outputs = atoi(strtok('\0', " "));
    if (!inputs || !hidden || !outputs)
      return 0;

    
    hiddenWeights = (double**) malloc(sizeof(double*) * hidden);
    for (i = 0; i < hidden; i++)
      hiddenWeights[i] = (double*) malloc(sizeof(double) * (inputs + 1));
    hiddenNeurons = (double*) malloc(sizeof(double) * hidden);

    // Aloca memoria para a camada de saida
    outputWeights = (double**) malloc(sizeof(double*) * outputs);
    for (i = 0; i < outputs; i++)
      outputWeights[i] = (double*) malloc(sizeof(double) * (hidden + 1));

    // Carrega os pesos da camada oculta
    for (i = 0; i < hidden && !feof(fp); i++) {
      fgets(line, LINE_SIZE_LIMIT, fp);
      hiddenWeights[i][0] = atof(strtok(line, " "));
      for (j = 1; j <= inputs && szPalavra; j++) {
        hiddenWeights[i][j] = atof(strtok('\0', " "));
      }
      if (j <= inputs) {
        fclose(fp);
        return 0;
      }
    }
    if (i < hidden) {
      fclose(fp);
      return 0;
    }

    // Carrega os pesos da camada de saida
    for (i = 0; i < outputs && !feof(fp); i++) {
      fgets(line, LINE_SIZE_LIMIT, fp);
      outputWeights[i][0] = atof(strtok(line, " "));
      for (j = 1; j <= hidden && szPalavra; j++) {
        outputWeights[i][j] = atof(strtok('\0', " "));
      }
      if (j <= hidden) {
        fclose(fp);
        return 0;
      }
    }
    fclose(fp);
    return (i < outputs ? 0 : 1);
}


void activateAnn(const double *inputLayer, double *outputLayer){
  register int i, j; // WTH????? do u have any notion HOW DANGEROUS IS THIS?? O.o'

  // Ativa a camada oculta
  for (i = 0; i < hidden; i++) {
    hiddenNeurons[i] = hiddenWeights[i][inputs];
    for (j = 0; j < inputs; j++){
      hiddenNeurons[i] += inputLayer[j] * hiddenWeights[i][j];
    }
    hiddenNeurons[i] = tanh(hiddenNeurons[i]);
  }

  // Ativa as saidas lineares
  for (i = 0; i < outputs; i++) {
    outputLayer[i] = outputWeights[i][hidden];
    for (j = 0; j < hidden; j++)
      outputLayer[i] += hiddenNeurons[j] * outputWeights[i][j];
  }
}


void releaseAnn(){
  int i;

  // Desaloca a memoria da camada oculta
  if (hiddenNeurons != NULL) {
    free(hiddenNeurons);
    hiddenNeurons = NULL;
  }
  if (hiddenWeights != NULL) {
    for (i = 0; i < hidden; i++) {
      if (hiddenWeights[i] != NULL) {
        free(hiddenWeights[i]);
        hiddenWeights[i] = NULL;
      }
    }
    free(hiddenWeights);
    hiddenWeights = NULL;
  }

  // Desaloca a memoria da camada de saida
  if (outputWeights != NULL) {
    for (i = 0; i < outputs; i++) {
      if (outputWeights[i] != NULL) {
        free(outputWeights[i]);
        outputWeights[i] = NULL;
      }
    }
    free(outputWeights);
    outputWeights = NULL;
  }
}