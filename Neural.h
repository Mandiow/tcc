
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//************************************** Constantes **********************************************
#define LINE_SIZE_LIMIT 1024


//********************************** Variaveis globais *******************************************
static int inputs = 0;
static int hidden = 0;
static int outputs = 0;
static double **hiddenWeights = NULL;
static double *hiddenNeurons = NULL;
static double **outputWeights = NULL;



int initializeAnn(const char *weightsFile);
void activateAnn(const double *inputLayer, double *outputLayer);
void releaseAnn();
