#include <time.h>
#include <iostream>
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void laplacianFilters(double* channels);

void laplacianFilters(double* channels){
    /*<< 2 might work*/
    channels[2] = (channels[2] *4) - channels[1] - channels[3] - channels[9] - channels[11]; //FC1 = 4*FC1 - F3 - Fz - C3 - Cz
    channels[4] = (channels[4] *4) - channels[3] - channels[5] - channels[11] - channels[13]; //FC2 = 4*FC2 - Fz - F4 - Cz - F4
    channels[9] = (channels[9] *4) - channels[0] - channels[2] - channels[8] - channels[10]; // C3 = 4*C3 - FC5 - FC1 - CP5 - CP1
    channels[11] = (channels[11] *4) - channels[2] - channels[4] - channels[10] - channels[12]; // Cz = 4*Cz - FC1 - FC2 -CP1 - CP2
    channels[13] = (channels[13] *4) - channels[4] - channels[6] - channels[12] - channels[7]; // C4 = 4*C4 - FC2 - FC6 - CP2 - CP6
}



int main(int argc, char const *argv[]){
	FILE *fp,*fp2;
	const char *token = ",";
	double channels[14]; // Can be max of 16 chan
	double laplacianFilter[5]; // Can be max of 16 chan
	char *key;
	char * line = NULL;
    size_t len = 0;
    ssize_t read;
	fp=fopen("timeDataSet-bp.txt", "r");
	fp2=fopen("parsed.txt", "w");
	
    while ((read = getline(&line, &len, fp)) != -1) {
    	int i = 0;
    	printf("line:%s\n",line);
    	key = strtok(line, token);
    	channels[i] = atof(key);
    	printf("%f ",channels[i] );
    	i++
	 	while( (key = strtok(NULL, token)) != NULL ) {
	        channels[i] = atof(key);
	        printf("%f ",channels[i] );
	        i++;
	    }
	    printf("\n ");
	    laplacianFilters(channels);
        laplacianFilter[0] = channels[2];
        laplacianFilter[1] = channels[4];
        laplacianFilter[2] = channels[9];
        laplacianFilter[3] = channels[11];
        laplacianFilter[4] = channels[13];
	    for (i = 0; i < 5; ++i){
	    	printf("%f\n",laplacianFilter[i] );
	    	fprintf(fp2, " %d:%f",i+1,laplacianFilter[i]);
	    }
	    fprintf(fp2, "\n",i+1,laplacianFilter[i]);
	}
	fclose(fp);
	fclose(fp2);
	return 0;
}
