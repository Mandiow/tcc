#include <stdio.h>

#include <windows.h>
#include <stdlib.h>
#include <time.h>       /* time */
INPUT __initKey__();
void move(INPUT input,WORD command);
void releaseKey(INPUT input,WORD command);
void releaseAllKeys(INPUT input);
void protocolCountDown(INPUT input,WORD command);

					// A    D     S    W    E
static WORD keys[5] ={0x41,0x44,0x53,0x57,0x45};

int main(int argc, char const *argv[]) {
	INPUT keyboard;
	keyboard = __initKey__();
	WORD key = 0x4C;
	system("cls");
  	printf("Realize o que for pedido e pare apenas quando a nova instrucao for pedida\n");
  	printf("Cada atividade deve durar, ao menos 15 segundos, se preferir, repita-a\n");
  	//system("pause");
  	system("cls");
  	Sleep(7000);
	printf("Quando a contagem acabar\nRelaxe e atente a sua respiracao\n");
	protocolCountDown(keyboard,keys[1]);
	printf("Quando a contagem acabar\nFique abrindo e fechando a sua mão\n");
	protocolCountDown(keyboard,keys[2]);
	move(keyboard,key);

	printf("Obrigado\n");
	return 0;
}


INPUT __initKey__() {
	// This part needs to be moved
	INPUT input; // If I decide to go C++ is in = new INPUT();
	memset(&input,0,sizeof(input));
	input.type = INPUT_KEYBOARD;
	return input;
}

void move(INPUT input,WORD command) {
	input.ki.wVk = command;
	SendInput(1,&input,sizeof(input));
}

void releaseKey(INPUT input,WORD command){
	input.ki.wVk = command;
	input.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1,&input,sizeof(input));
}

void protocolCountDown(INPUT input,WORD command){
	time_t start, end;
	releaseAllKeys(input);
	Sleep(5000);
	printf("Em 3...\n");
	Sleep(1000);
	printf("Em 2...\n");
	Sleep(1000);
	printf("Em 1...\n");
	Sleep(1000);
	printf("Agora.\n");
	Sleep(800);
	move(input,command);
	time(&start);
	do time(&end); while(difftime(end, start) <= 15);
	releaseAllKeys(input);
	system("cls");
}

void releaseAllKeys(INPUT input){
	for (int i = 0; i < 5; ++i){
		releaseKey(input,keys[i]);
	}
}