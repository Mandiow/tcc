//**********************************************************
//AUTHOR: Caiã de Aragão Frazão
//The use of this file is to connect the OpenBCI GUI and make some stuff
//BE CAREFUL
//**********************************************************
#include <winsock2.h>
#include <time.h>
#include "svm.h"
#include "Iir.h"
//#include "mtpsd.h"
#include <windows.h>
#include <stdio.h>
#include <iostream>
#include <inttypes.h>
#include <cwchar>
#include <string>
//#pragma comment(lib,"WS2_32.lib") //Winsock Library, is in the folder
 
#define SERVER "127.0.0.1"  //ip address of udp server
#define BUFLEN 65507  //Max length of buffer

typedef int Boolean;
struct sockaddr_in si_other;
WSADATA wsa;
int eventMarker;
int s;
struct svm_node *x;
int max_nr_attr = 128;
struct svm_model* model;
int numberOfChannels = 0;
void connectToBCIServer(int port);
void powerDifference(double** powerBandInputs,int channel);
void printUsage();
void fontSize(int size);
void predict(struct svm_model *model,double *input,double *output,int finalIndex);
void predictTS(struct svm_model *model,double *input,double *output,int finalIndex);
void predictEMG(struct svm_model *model,double *input,double *output);
void sendMessage(const char *msg);
void laplacianFilter(double* channels);
void filterReceivedInputs(double* channels,double** filteredChannels);
#define PD(x,y) ((x-y)/(x+y))
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)
/*
Notes: 
to compile in sublime USE:  "cmd" : ["gcc", "$file_names","-lWS2_32", "-o", "${file_base_name}.exe"]
I'll use a MLP with one hidden layer only to test purposes.

Input: A vector with [N] inputs that are the channels
Output: A vector with [N] binary representations of the pressed key/desired moviment

e.g.:
Input: [-66.071754,3.1962993,3.5315754,-0.17881395,-14.595689,12.762846,-3.486872,-15.310945]
Output: [0,0,0,1] - This represents that 'D' key is being pressed
After some time:
Input: [3.3527615,-15.4897585,-57.891018,56.438152,6.28084,-5.498529,2.8610232,-16.428532]
Output: [0,0,0,0] - This represents key that 'D' was released

*/

#define TRUE 1
#define FALSE 0
/*
To press a key you need to use SendInput(Num of inputs, Pointer to INPUT struct, Sizeof the structure)
UINT WINAPI SendInput(_In_ UINT    nInputs, _In_ LPINPUT pInputs, _In_ int     cbSize);
SHORT WINAPI VkKeyScan(_In_ TCHAR ch);

To release use ki.dwFlags = KEYEVENTF_KEYUP;

*/


//I have to redeclare windows root structures, this is such bullshit

typedef struct _CONSOLE_FONT_INFOEX
{
    ULONG cbSize;
    DWORD nFont;
    COORD dwFontSize;
    UINT  FontFamily;
    UINT  FontWeight;
    WCHAR FaceName[LF_FACESIZE];
}CONSOLE_FONT_INFOEX, *PCONSOLE_FONT_INFOEX;

//the function declaration begins
#ifdef __cplusplus
extern "C" {
#endif
//void fastICA(double** X, int rows, int cols, int compc, double** K, double** W, double** A, double** S);
BOOL WINAPI SetCurrentConsoleFontEx(HANDLE hConsoleOutput, BOOL bMaximumWindow, PCONSOLE_FONT_INFOEX lpConsoleCurrentFontEx);
BOOL WINAPI GetCurrentConsoleFont(HANDLE hConsoleOutput, BOOL bMaximumWindow, PCONSOLE_FONT_INFO lpConsoleCurrentFont);
BOOL WINAPI SetConsoleDisplayMode(HANDLE hConsoleOutput, DWORD  dwFlags, PCOORD lpNewScreenBufferDimensions);
BOOL WINAPI SetConsoleCursorPosition(HANDLE hConsoleOutput,COORD  dwCursorPosition);
#ifdef __cplusplus
}
#endif
