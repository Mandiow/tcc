//**********************************************************
//AUTHOR: Caiã de Aragão Frazão
//The use of this file is to simule a key press
//BE CAREFUL
//**********************************************************
#include "key.h"

INPUT __initKey__() {
	// This part needs to be moved
	INPUT input; // If I decide to go C++ is in = new INPUT();
	memset(&input,0,sizeof(input));
	input.type = INPUT_KEYBOARD;
	return input;
}

void pressKey(INPUT input[],int size) {
	SendInput(size,input,sizeof(INPUT));
}

void releaseKey(INPUT input,WORD command){
	input.ki.wVk = command;
	input.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1,&input,sizeof(input));
}