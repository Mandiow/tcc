
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0
/*
To press a key you need to use SendInput(Num of inputs, Pointer to INPUT struct, Sizeof the structure)
UINT WINAPI SendInput(_In_ UINT    nInputs, _In_ LPINPUT pInputs, _In_ int     cbSize);
SHORT WINAPI VkKeyScan(_In_ TCHAR ch);

To release use ki.dwFlags = KEYEVENTF_KEYUP;

*/					// A    D     S    W    E
static WORD keys[5] ={0x41,0x44,0x53,0x57,0x45};

INPUT __initKey__();
void pressKey(INPUT input[], int size);
void releaseKey(INPUT input,WORD command);
