#include "client.h"
#include <sys\timeb.h> 
#define PORT 22348   //The port on which to listen for incoming data
/*
A good disclaimer, this code is mostly a big chunk of lines to not create overhead, I NEED it fast. If you can optimize my mess, fell free and thanks in advance! :)

To build:
g++ -O3 svm.cpp iir/Bessel.cpp iir/Biquad.cpp iir/Butterworth.cpp iir/Cascade.cpp iir/ChebyshevI.cpp iir/ChebyshevII.cpp iir/Custom.cpp iir/Elliptic.cpp iir/Legendre.cpp iir/PoleFilter.cpp iir/RBJ.cpp iir/RootFinder.cpp iir/State.cpp client.c -lWS2_32 -o client.exe

And yeah, i know this is a monstrous line, I'm using windows and do not have much time, so, please, accept this and if feel free to make a Cmake or makefile that actually works in the OS
To call:
client.exe -c 16 -mode svm -ts

*/
/*
Notes: 
to compile in sublime USE:  "cmd" : ["gcc", "$file_names","-lWS2_32", "-o", "${file_base_name}.exe"]
I'll use a MLP with one hidden layer only to test purposes.

Input: A vector with [N] inputs that are the channels
Output: A vector with [N] binary representations of the pressed key/desired moviment

e.g.:
Input: [-66.071754,3.1962993,3.5315754,-0.17881395,-14.595689,12.762846,-3.486872,-15.310945]
Output: [0,0,0,0.56353] - This represents that 'D' key is being pressed, since it passed the THRESHOLD
After some time:
Input: [3.3527615,-15.4897585,-57.891018,56.438152,6.28084,-5.498529,2.8610232,-16.428532]
Output: [0,0,0,0] - This represents key that 'D' was released

*/
Boolean captureData = FALSE;
Boolean testPrint = FALSE;
Boolean useSVM = FALSE;
Boolean powerDifferenceEnabled = FALSE;
Boolean debug = FALSE;
Boolean timeSeries = FALSE;
Boolean emg = FALSE;
Boolean powerBand = FALSE;
Boolean laplace = FALSE;
Boolean DELTA = FALSE,THETA = FALSE,ALPHA_BAND = FALSE,BETA = FALSE,GAMMA = FALSE;
Boolean started = FALSE;
Boolean protocol = FALSE;
Boolean channelSelection = FALSE;
Boolean testUDP = FALSE;
Boolean useFilters = FALSE, nativeBandPower = FALSE;
Iir::Butterworth::LowPass<2> lowPass[16][5];
Iir::Butterworth::HighPass<2> highPass[16][5];
Iir::Butterworth::BandStop<2,Iir::DirectFormI> bs[16][5];
int bandAmount = 0;
DWORD WINAPI ThreadFunc( LPVOID lpParam ) 
{

    int     Data = 0;
    int     count = 0;
    HANDLE  hStdout = NULL;
    COORD cursor = { 25, 10 };
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    int ret;
    
    ret = GetConsoleScreenBufferInfo(GetStdHandle( STD_OUTPUT_HANDLE ),&csbi);
    CONSOLE_CURSOR_INFO info = {0};
    // Get Handle To screen.
    // Else how will we print?
    if( (hStdout = 
         GetStdHandle(STD_OUTPUT_HANDLE)) 
         == INVALID_HANDLE_VALUE )  
    return 1;
    info.dwSize = 100;
    info.bVisible = FALSE;
    SetConsoleCursorInfo(hStdout, &info);
    // Cast the parameter to the correct
    // data type passed by callee i.e main() in our case.
    
    //MessageBeep(MB_ICONQUESTION);    /* play question sound */
    //MessageBeep(MB_ICONWARNING);     /* play warning sound */
    cursor.X = 19;
    cursor.Y = 5;
    Sleep(5000);
    for(int p = 0 ; p < 3 ; ++p){
        SetConsoleCursorPosition(hStdout,cursor);
        MessageBeep(MB_OK);
        printf("RELAX");
        Sleep(300);
        eventMarker = 1;
        Sleep(60000);
        system("cls");
        SetConsoleCursorPosition(hStdout,cursor);
        MessageBeep(MB_OK);
        printf("<---");
        Sleep(300);
        eventMarker = 2;
        Sleep(120000);
        system("cls");
        SetConsoleCursorPosition(hStdout,cursor);
        MessageBeep(MB_OK);
        printf("RELAX");
        Sleep(300);
        eventMarker = 1;
        Sleep(60000);
        system("cls");
        SetConsoleCursorPosition(hStdout,cursor);
        MessageBeep(MB_OK);
        printf("+");
        Sleep(300);
        eventMarker = 3;
        Sleep(120000);
        system("cls");
    }
    fontSize(16);
    SetConsoleDisplayMode( hStdout, 0, &cursor);
    captureData = FALSE;
    
    return 0; 
} 

/*
 * Creates a matrix
 */
static double **mat_create(int rows, int cols)
{
    double **M; int i;
    
    M = (double**) malloc(rows * sizeof(double*));
    for (i=0; i<rows; i++)
        M[i] = (double*) malloc(cols * sizeof(double));
    
    return M;
}

/*
 * Reads matrix M from fp
 */
static double **mat_read(FILE *fp, int *rows, int *cols)
{
    int i, j; double **M;

    fscanf(fp, "%d %d", rows, cols);
    M = mat_create(*rows, *cols);
    
    for (i=0; i<*rows; i++) {
        for (j=0; j<*cols; j++)
            fscanf(fp, "%lf ", &(M[i][j])); 
    }
    
    return M;   
}

/*
 * Prints matrix M to stdout
 */
static void mat_print(double **M, int rows, int cols)
{
    int i, j;

    for (i=0; i<rows; i++) {
        for (j=0; j<cols; j++) {
            printf("%0.6f", M[i][j]);
            if (j < cols - 1)
                printf(" ");
        }
        printf("\n");
    }
}


int main(int argc, char const *argv[]){
    double channels[16],laplacianFilters[5],**filteredInputs; // Can be max of 16 chan
    int chosenChannels[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    double ***timeLapseToPSD;
    int event = 0;
    int port = 22348;
    int step = 1;
    //This var is a loop control to handle the spaceBar pressing, I DESPERADELY NEED A BETTER WAY
    //fastICA(NULL, 0, 0, 0, NULL, NULL, NULL, NULL);
    int cicleControl = 0;
    char *key;
    const char *token = ",";
    int slen=sizeof(si_other);
    
    char *buf = new char[BUFLEN];
    char *filename = new char[BUFLEN];
    
    unsigned long long int line = 1;
    if(0){
        double **X, **K, **W, **A, **S;
    int rows, cols, compc;
    FILE *fp;

    // Input parameters check
    if (argc == 2) {
        if ((fp = fopen(argv[1], "r")) == NULL) {
            perror("Error opening input file");
            exit(-1);
        }
        } else {    
            printf("usage: %s data_file\n", argv[0]);
            exit(-1);
        }

        compc = 2;
        
        // Matrix creation
                        //TIME  //components
        X = mat_read(fp, &rows, &cols);
        W = mat_create(compc, compc);
        A = mat_create(compc, compc);
        K = mat_create(cols, compc);
        S = mat_create(rows, cols); 
        // X[TIME][features]
        // W[features][features]
        // A[features][features]
        // K[features][features]
        // S[TIME][features]
        // ICA computation
        //fastICA(X, rows, cols, compc, K, W, A, S);

        // Output
        printf("$K\n"); 
        mat_print(K, cols, compc);

        printf("\n$W\n");
        mat_print(W, compc, compc);

        printf("\n$A\n");
        mat_print(A, compc, compc);
        
        printf("\n$S\n");
        mat_print(S, rows, compc);
    }
    if(argc > 1){
        char bandToUse[30];
        bzero(bandToUse,30);
        for (int i = 1; i < argc; ++i){
            printf("%s\n",argv[i] );
            if(strstr(argv[i],"-d")){
                debug = TRUE;
            }
            if(strcmp(argv[i],"-udp") == 0){
                testUDP = TRUE;
                break;
            }
            if(strcmp(argv[i],"-laplace") == 0){
                laplace = TRUE;
            }
            if(strcmp(argv[i],"-nativeBP") == 0){
                nativeBandPower = TRUE;
            }
            if(strcmp(argv[i],"-protocol") == 0){
                protocol = TRUE;
            }
            if(strcmp(argv[i],"-useFilters") == 0){
                useFilters = TRUE;
                printf("Please, specify which bands you want to use:\nWrite like DELTA,THETA,ALPHA,BETA,GAMMA\n");
                scanf("%s",&bandToUse);
                printf("%s\n",bandToUse );
                char *chosen = strtok(bandToUse,token);
                printf("%s\n",chosen );
                if(chosen != NULL){
                    if(strcmp("DELTA",chosen) == 0){
                        DELTA = TRUE;
                    }
                    if(strcmp("THETA",chosen) == 0){
                        THETA = TRUE;
                    }
                    if(strcmp("ALPHA",chosen) == 0){
                        ALPHA_BAND = TRUE;
                    }
                    if(strcmp("BETA",chosen) == 0){
                        BETA = TRUE;
                    }
                    if(strcmp("GAMMA",chosen) == 0){
                        GAMMA = TRUE;
                    }
                }
                while( (chosen = strtok(NULL, token)) != NULL ) {
                    if(strcmp("DELTA",chosen) == 0){
                        DELTA = TRUE;
                    }
                    if(strcmp("THETA",chosen) == 0){
                        THETA = TRUE;
                    }
                    if(strcmp("ALPHA",chosen) == 0){
                        ALPHA_BAND = TRUE;
                    }
                    if(strcmp("BETA",chosen) == 0){
                        BETA = TRUE;
                    }
                    if(strcmp("GAMMA",chosen) == 0){
                        GAMMA = TRUE;
                    }
                }
                printf("%d%d%d%d%d\n",DELTA,THETA,ALPHA_BAND,BETA,GAMMA);
                if(!DELTA && !THETA && !ALPHA_BAND && !BETA && !GAMMA){
                    printf("You need to specify at least one valid band\n");
                    return -1;
                }
            }
            if(strstr(argv[i],"-port")){
                port = atoi(argv[i+1]);
                printf("%d\n",port );
            }
            if(strstr(argv[i],"-mode")){

                if (strstr(argv[i+1],"capture")){
                    
                    captureData = TRUE;
                }else if (strstr(argv[i+1],"svm")){
                    useSVM = TRUE;
                }else if (strstr(argv[i+1],"testPrint")){
                    testPrint = TRUE;
                }
            }
            if(strstr(argv[i],"-pb")){
                powerBand = TRUE;
                if(timeSeries || emg)
                    return -1;
                printf("Please, specify which bands you want to use:\nWrite like DELTA,THETA,ALPHA,BETA,GAMMA\n");
                scanf("%s",&bandToUse);
                printf("%s\n",bandToUse );
                char *chosen = strtok(bandToUse,token);
                printf("%s\n",chosen );
                if(chosen != NULL){
                    if(strcmp("DELTA",chosen) == 0)
                        DELTA = TRUE;
                    if(strcmp("THETA",chosen) == 0)
                        THETA = TRUE;
                    if(strcmp("ALPHA",chosen) == 0)
                        ALPHA_BAND = TRUE;
                    if(strcmp("BETA",chosen) == 0)
                        BETA = TRUE;
                    if(strcmp("GAMMA",chosen) == 0)
                        GAMMA = TRUE;
                }
                while( (chosen = strtok(NULL, token)) != NULL ) {
                    if(strcmp("DELTA",chosen) == 0)
                        DELTA = TRUE;
                    if(strcmp("THETA",chosen) == 0)
                        THETA = TRUE;
                    if(strcmp("ALPHA",chosen) == 0)
                        ALPHA_BAND = TRUE;
                    if(strcmp("BETA",chosen) == 0)
                        BETA = TRUE;
                    if(strcmp("GAMMA",chosen) == 0)
                        GAMMA = TRUE;
                }
                if(!DELTA && !THETA && !ALPHA_BAND && !BETA && !GAMMA){
                    printf("You need to specify at least one valid band\n");
                    return -1;
                }


            }
            if(strcmp(argv[i],"-ts") == 0){
                timeSeries = TRUE;
                if(powerBand || emg)
                    return -1;
            }
            if(strcmp(argv[i],"-emg") == 0){
                emg = TRUE;
                if(powerBand || timeSeries)
                    return -1;
            }
            if(strcmp(argv[i],"-cs") == 0){
                channelSelection = TRUE;
                int chan = 0; 
                printf("Please, specify which channels you want to record as a feature:\n Type each channel at the time\n0 to finish -1 for all\n");
                do{
                    scanf("%d",&chan);
                    if(chan > 0){
                        chosenChannels[chan-1] = 1;

                    }if(chan == -1){
                        for(chan = 0 ; chan < numberOfChannels ; ++chan){
                            chosenChannels[chan] = 1;                            
                        }
                        break;
                    }
                }while(chan !=0);
            }
            if(strcmp(argv[i],"-c") == 0){
                if(i+1 < argc){
                    numberOfChannels = atoi(argv[i+1]);
                    if(numberOfChannels > 16){
                        printUsage();
                        return -1;
                    }
                }
            }
        }
    }
    if (!channelSelection){
         for(int chan = 0 ; chan < numberOfChannels ; ++chan){
            chosenChannels[chan] = 1;                            
        }
    }
    //I'm a genius, but not LOL
    bandAmount = DELTA + THETA + ALPHA_BAND + GAMMA + BETA;
    if(!testPrint && !testUDP)
        if(!numberOfChannels || (!timeSeries && !emg && !powerBand) || (!captureData && !useSVM)){
            printUsage();
            return -1;
        }
    double *BCIinputs;
    double *KEYoutputs;
    double **powerBandInputs; 
    FILE *fp,*fp2;
    if(useFilters){
        if(nativeBandPower){
            timeLapseToPSD =(double***) malloc(sizeof(double**) * numberOfChannels);
            for (int a = 0; a < numberOfChannels; ++a){
                 timeLapseToPSD[a] = (double**) malloc(sizeof(double*) * 5);
                 for (int b = 0; b < bandAmount; ++b){
                     timeLapseToPSD[a][b] = (double*) malloc(sizeof(double) * 5);//window of 5, why? because I said so u.u
                 }
            }
        }
        const float samplingrate = 125; // Hz, OpenBCI 16-chan
        const float centerFrequency = 28; // Hz
        const double widthFrequency = 22;
        /*(int order,double sampleRate,double centerFrequency,double widthFrequency);*/
        for (int i = 0; i < 16; ++i){
            lowPass[i][0].setup(2, samplingrate, 4);
            highPass[i][0].setup(2, samplingrate, 0.5);
            bs[i][0].setup(2, samplingrate, 60,2);
            lowPass[i][1].setup(2, samplingrate, 7);
            highPass[i][1].setup(2, samplingrate, 4);
            bs[i][1].setup(2, samplingrate, 60,2);
            lowPass[i][2].setup(2, samplingrate, 12);
            highPass[i][2].setup(2, samplingrate, 8);
            bs[i][2].setup(2, samplingrate, 60,2);
            lowPass[i][3].setup(2, samplingrate, 30);
            highPass[i][3].setup(2, samplingrate, 12);
            bs[i][3].setup(2, samplingrate, 60,2);
            lowPass[i][4].setup(2, samplingrate, 50);
            highPass[i][4].setup(2, samplingrate, 30);
            bs[i][4].setup(2, samplingrate, 60,2);
        }
        
        //bp.setup (2, samplingrate, centerFrequency, widthFrequency);
    }
    if(captureData && powerBand){        
        connectToBCIServer(port);
        fp=fopen("C:\\powerDataSet.txt", "w");
        fp2=fopen("C:\\powerDataSetWithDesiredOutputs.txt", "w");
    } 
    if(captureData && timeSeries){        
        connectToBCIServer(port);
        fp=fopen("C:\\timeDataSet.txt", "w");
        fp2=fopen("C:\\timeDataSetWithDesiredOutputs.txt", "w");
    }
    if(captureData && emg){        
        connectToBCIServer(port);
        fp=fopen("C:\\emgDataSet.txt", "w");
        fp2=fopen("C:\\emgDataSetWithDesiredOutputs.txt", "w");
    } 
    if(emg){
        BCIinputs = (double*) malloc(sizeof(double) * 2);
    }
    if(powerBand){
        BCIinputs = (double*) malloc(sizeof(double) * numberOfChannels*bandAmount);
        powerBandInputs =(double**) malloc(sizeof(double*) * numberOfChannels);
        if(powerDifferenceEnabled){
                for (int i = 0; i < numberOfChannels; i++)
                    powerBandInputs[i] = (double*) malloc(sizeof(double) * numberOfChannels*bandAmount);
        }else{
            for (int i = 0; i < numberOfChannels; i++)
                powerBandInputs[i] = (double*) malloc(sizeof(double) * bandAmount);
        }
    }
    if(timeSeries){
        if(bandAmount)
            BCIinputs = (double*) malloc(sizeof(double) * numberOfChannels*bandAmount);
        else
            BCIinputs = (double*) malloc(sizeof(double) * numberOfChannels);
        filteredInputs = (double**) malloc(sizeof(double*) * numberOfChannels);
        for (int i = 0; i < numberOfChannels; ++i){
            filteredInputs[i] = (double*) malloc(sizeof(double) * 5);
        }
    }
    
    KEYoutputs = (double*) malloc(sizeof(double));
    if(useSVM){
        connectToBCIServer(port);
        int j;
        x = (struct svm_node *) malloc(max_nr_attr*sizeof(struct svm_node));
        if(powerBand || timeSeries){
            if((model=svm_load_model("eeg.data.model"))==0){
                fprintf(stderr,"can't open model file\n");
                exit(1);
            }
        }else{
            if((model=svm_load_model("emg.data.model"))==0){
                fprintf(stderr,"can't open model file\n");
                exit(1);
            }
		}
        
    }
    
    while(useSVM){
        //Receive message
        //strcpy(buf,"[[67.69558,873.02344,83.17772,151.30247,131.14465],[37.717,699.9358,69.51988,128.18307,113.81166],[39.8543,736.82733,78.77106,134.58551,117.83911],[17.022417,499.88272,38.425323,92.305405,91.67035],[10.0464325,534.57007,38.09386,90.07715,91.36044],[0.804133,127.4574,3.8452852,14.759868,23.321987],[32.947163,650.2743,52.22373,117.00754,105.054306],[93.13989,1060.1652,93.61812,172.0973,141.20903],[410.34283,1503.0592,276.31918,312.72083,201.84933],[23.188759,99.0454,16.767912,17.542273,11.375912],[4.5577106,21.610687,8.495249,6.413058,3.931048],[22.948223,274.36823,69.95364,115.59658,108.44125],[19.22974,268.46805,70.6855,109.552795,94.67353],[11.405738,44.07682,10.522983,9.874128,5.59763],[10.630745,56.007744,16.012032,15.433195,9.970869],[22.391253,266.63406,50.676983,63.0283,51.556255]]}");
        if (recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen) == SOCKET_ERROR){
            printf("recvfrom() failed with error code : %d" , WSAGetLastError());
            exit(EXIT_FAILURE);
        }
        if(debug){
            printf("%s\n",buf );
        }
        //Parses to doubles
        int i = 0;
        int j = 0;
        char trueBuf[BUFLEN];
        if (emg){
             while(buf[i] != '['){
                i++;
            }
            i++;
            j = 0;
            memset(trueBuf,0,BUFLEN);
            while(buf[i] != ']'){// works only with Time series
                trueBuf[j] = buf[i];
                i++;
                j++;
            }
            i = 0;
                /* get the first token */
            strcpy(buf,trueBuf);
            
            ++line;
            
            key = strtok(buf, token);
            channels[i] = atof(key);
            
            if(debug){
                printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
            }
            i++;
            /* walk through other tokens */
            while( (key = strtok(NULL, token)) != NULL ) {
                
                channels[i] = atof(key);
                if(debug){
                    printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
                }
                i++;
            }
            //Threshold implementation
            if(channels[0] > 0.6 && channels[1] > 0.6){
                KEYoutputs[0] = 2;
            }else
                KEYoutputs[0] = 1;
            //SVM implementation
            //BCIinputs[0] = channels[0];
            //BCIinputs[1] = channels[1];
            //predict(model,BCIinputs,KEYoutputs);
            
        }
        if(timeSeries){
            while(buf[i] != '['){
                i++;
            }
            i++;
            memset(trueBuf,0,BUFLEN);
            while(buf[i] != ']'){// works only with Time series
                trueBuf[j] = buf[i];
                i++;
                j++;
            }
            trueBuf[i+1] = '\n';
            trueBuf[i+2] = '\0'; 
            i = 0;
            /* get the first token */
            key = strtok(trueBuf, token);
            channels[i] = atof(key);
            i++;
            /* walk through other tokens */
            while( (key = strtok(NULL, token)) != NULL && i < numberOfChannels) {
                
                channels[i] = atof(key);
                //printf("CHANNEL %d: %.6f TOKEN: %s",i,channels[i],token ); // To see channels output
                i++;
            }
            int feature = 0;
            if(laplace){
                laplacianFilter(BCIinputs);
                laplacianFilters[0] = BCIinputs[2];
                laplacianFilters[1] = BCIinputs[4];
                laplacianFilters[2] = BCIinputs[9];
                laplacianFilters[3] = BCIinputs[11];
                laplacianFilters[4] = BCIinputs[13];
                predictTS(model,laplacianFilters,KEYoutputs,5);
            }else{ 
                if(useFilters){
                    filterReceivedInputs(channels,filteredInputs);
                    if(0){
                      /*  for (int a = 0; a < numberOfChannels; ++a){
                            if(THETA){
                                timeLapseToPSD[a][0][step-1] = filteredInputs[a][0];
                                
                            }
                            if(DELTA){
                                timeLapseToPSD[a][1][step-1] = filteredInputs[a][1];
                            }
                            if(ALPHA_BAND){
                                timeLapseToPSD[a][2][step-1] = filteredInputs[a][2];
                            }
                            if(BETA){
                                timeLapseToPSD[a][3][step-1] = filteredInputs[a][3];
                            }
                            if(GAMMA){
                                timeLapseToPSD[a][4][step-1] = filteredInputs[a][4];
                            }
                        }
                        ++step;
                        if(step == 5){
                            for (int a = 0; a < numberOfChannels; ++a){
                                if(THETA){
                                    mtpsd<double> spectrum(timeLapseToPSD[a][0], 5, 100);//This last value i do not get
                                    spectrum.compute();
                                    printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                    //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                    ++feature;
                                }
                                if(DELTA){
                                    mtpsd<double> spectrum(timeLapseToPSD[a][1], 5, 2);//This last value i do not get
                                    spectrum.compute();
                                    printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                    //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                    ++feature;
                                }
                                if(ALPHA_BAND){
                                    mtpsd<double> spectrum(timeLapseToPSD[a][2], 5, 2);//This last value i do not get
                                    spectrum.compute();
                                    printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                    //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                    ++feature;
                                }
                                if(BETA){
                                    mtpsd<double> spectrum(timeLapseToPSD[a][3], 5, 2);//This last value i do not get
                                    spectrum.compute();
                                    printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                    //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                    ++feature;
                                }
                                if(GAMMA){
                                    mtpsd<double> spectrum(timeLapseToPSD[a][4], 5, 2);//This last value i do not get
                                    spectrum.compute();
                                    printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                    //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                    ++feature;
                                }
                                

                            }
                            step = 1;
                        }*/
                    }else{
                        for (int a = 0; a < numberOfChannels; ++a){
                            if(DELTA){
                                if(chosenChannels[a]){
                                    BCIinputs[feature] = filteredInputs[a][0];
                                    ++feature;
                                }
                            }
                            if(THETA){
                                if(chosenChannels[a]){
                                    BCIinputs[feature] = filteredInputs[a][1];
                                    ++feature;  
                                }
                            }
                            if(ALPHA_BAND){
                                if(chosenChannels[a]){
                                    BCIinputs[feature] = filteredInputs[a][2];
                                    ++feature;
                                }
                            }
                            if(BETA){
                                if(chosenChannels[a]){
                                    BCIinputs[feature] = filteredInputs[a][3];
                                    ++feature;
                                }
                            }
                            if(GAMMA){
                                if(chosenChannels[a]){
                                    BCIinputs[feature] = filteredInputs[a][4];
                                    ++feature;
                                }
                            }
                        }
                    }
                }else{
                     for (int a = 0; a < numberOfChannels; ++a){
                        if(chosenChannels[a]){
                            BCIinputs[feature] = channels[a];
                            ++feature;
                        }
                    }
                }
                if(debug){
                    for (int a = 0; a <= feature; ++a){
                          printf("FEATURE %d:%f\n",a,BCIinputs[a] );
                    }
                }
                predictTS(model,BCIinputs,KEYoutputs,feature);
            }
        }
        if (powerBand){
            
            //this is a JSON like structure, lets make it happen
            int channel = 0;
            while(buf[i] != ']' && channel < numberOfChannels){
                int freqBW = 0;
                while(buf[i] != '['){
                    i++;
                }
                //To point to the 1st number
                if(channel == 0)
                    i+=2;
                else
                    i++;
                j=0;
                memset(trueBuf,0,BUFLEN);
                while(buf[i] != ']'){
                    trueBuf[j] = buf[i];
                    i++;
                    j++;
                }
                key = strtok(trueBuf, token);
                if(DELTA && key != NULL){
                    powerBandInputs[channel][0] = atof(key);
                    freqBW++;
                }
                key = strtok(NULL, token);
                if(THETA && key != NULL){
                    powerBandInputs[channel][1] = atof(key);
                    freqBW++;;   
                }
                key = strtok(NULL, token);
                if(ALPHA_BAND && key != NULL){
                    powerBandInputs[channel][2] = atof(key);
                    freqBW++;
                }
                key = strtok(NULL, token);
                if(BETA && key != NULL){
                    powerBandInputs[channel][3] = atof(key);
                    freqBW++;
                }
                key = strtok(NULL, token);
                if(GAMMA && key != NULL){
                    powerBandInputs[channel][4] = atof(key);
                    freqBW++;
                }
                /*if(debug){
                    printf("CHANNEL %d freqBW %d: %.6f TOKEN: %s\n",channel,freqBW,powerBandInputs[channel][freqBW],key ); // To see channels output
                }
                // walk through other tokens 
                while( (key = strtok(NULL, token)) != NULL ) {
                    powerBandInputs[channel][freqBW] = atof(key);
                    if(debug){
                        printf("CHANNEL %d freqBW %d: %.6f TOKEN: %s\n",channel,freqBW,powerBandInputs[channel][freqBW],key ); // To see channels output
                    }
                    freqBW++;
                }*/
                //god help me
               
                channel++;
                i++;
            }
            //powerDifference(powerBandInputs,channel);
            int feature = 0;
                for (int a = 0; a < numberOfChannels; ++a){
                    if(DELTA){
                        if(chosenChannels[a]){
                            BCIinputs[feature] = powerBandInputs[a][0];
                            ++feature;
                        }
                    }
                    if(THETA){
                        if(chosenChannels[a]){
                            BCIinputs[feature] = powerBandInputs[a][1];
                            ++feature;  
                        }
                    }
                    if(ALPHA_BAND){
                        if(chosenChannels[a]){
                            BCIinputs[feature] = powerBandInputs[a][2];
                            ++feature;
                        }
                    }
                    if(BETA){
                        if(chosenChannels[a]){
                            BCIinputs[feature] = powerBandInputs[a][3];
                            ++feature;
                        }
                    }
                    if(GAMMA){
                        if(chosenChannels[a]){
                            BCIinputs[feature] = powerBandInputs[a][4];
                            ++feature;
                        }
                    }
                }
            if(debug){
                for (int a = 0; a <= feature; ++a){
                      printf("FEATURE %d:%f\n",a,BCIinputs[a] );
                }
            }
            predict(model,BCIinputs,KEYoutputs,feature);
        }
        printf("OUTPUT: %f\n",KEYoutputs[0]);
        if(KEYoutputs[0] == 1){
           sendMessage("Baseline");
        }
        if(KEYoutputs[0] == 2){
           sendMessage("Hand Movement");
        }
        if(KEYoutputs[0] == 3){
            sendMessage("Hand Imagery");
        }if(KEYoutputs[0] == 4){
            sendMessage("Arm Movement");
        }
        if(KEYoutputs[0] == 5){
            sendMessage("Arm Imagery");
        }
        if(KEYoutputs[0] == 6){
            sendMessage("Third Arm");
        }
        if(GetAsyncKeyState('L') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            
            useSVM = FALSE;
        }
        //Uses the MLP to get the output
        
        /*for (i = 0; i < 5; ++i){
            if (KEYoutputs[i] > 0.7){ //THIS IS A THRESHOLD
                keyboard[i].ki.wVk = keys[i];
                keyboard[i].ki.dwFlags = 0;
            }else{
                keyboard[i].ki.wVk = keys[i];
                keyboard[i].ki.dwFlags = KEYEVENTF_KEYUP;
            }
        }
        //Shows output
        pressKey(keyboard,5);*/
    }
    if(testPrint){
        fontSize(96);
        printf("This is a test mode to adjust the cursor position use 0 0 to exit\n");
        system("pause");
        system("cls");
    }
    while(testPrint){
        int X = 1;
        int Y = 2;
        COORD cursor = { 25, 10 };
        printf("X Y:");
        scanf("%d %d",&X,&Y);
        system("cls");
        if(!X && !Y){
            testPrint = FALSE;
        }
        cursor.X = X;
        cursor.Y = Y;
        SetConsoleCursorPosition(GetStdHandle( STD_OUTPUT_HANDLE ),cursor);
        printf("HERE\n");
        cursor.X = 1;
        cursor.Y = 2;
        SetConsoleCursorPosition(GetStdHandle( STD_OUTPUT_HANDLE ),cursor);
    }
	//While to capture data to use in the MLP trainer
    int record;
    if(captureData){
        fontSize(24);
        printf("Para iniciar a captura\n");
        system("pause");
        system("cls");
        fontSize(96);
        eventMarker = 0;
        if(protocol){
            HANDLE thread = CreateThread(NULL, 0, ThreadFunc, NULL, 0, NULL);
            if ( thread == NULL)
                ExitProcess(0);
        }
    }
        struct timeb start, end;
        ftime(&start);
    if(testUDP){
        connectToBCIServer(port); 
    }
    while(testUDP){
        if (recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen) == SOCKET_ERROR){
            printf("recvfrom() failed with error code : %d" , WSAGetLastError());
            exit(EXIT_FAILURE);
        }
        int i = 0;
        while(buf[i] != '}')
                i++;
            buf[i+1] = '\0';
        i = 0;
        printf("%s\n",buf );
        
    }
    while(captureData){
        int diff;
        record = FALSE;
        
        //receive a reply and print it
        //clear the buffer by filling null, it might have previously received data
        
        //try to receive some data, this is a blocking call
        //buf = "[[41.669846,49.689392,81.83225,366.5136,329.9111],[0.42361617,0.6239324,20.908672,2.0270483,2.7384207],[0.12620935,0.6780958,0.34697872,2.8156202,244.40503],[0.30882332,0.9020997,0.8557987,3.1498816,2.6902924],[0.34740353,0.5335196,0.885495,3.4016564,2.1252117],[0.2156948,0.4901576,0.38074055,3.1702325,2.386418],[0.158955,0.50894904,0.58843136,1.7801483,3.2499516],[0.25537622,0.54603285,0.3565757,3.4537523,2.4447072]]";
        if (recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen) == SOCKET_ERROR){
            printf("recvfrom() failed with error code : %d" , WSAGetLastError());
            exit(EXIT_FAILURE);
        }
        if(debug){
            printf("%s\n",buf );
        }
        int i = 0;
        int j = 0;
        char trueBuf[BUFLEN];
        fprintf(fp2, "%d ",eventMarker);
        if(emg){
            while(buf[i] != '}')
                i++;
            buf[i+1] = '\0';
            i = 0;
            while(buf[i] != '['){
                i++;
            }
            i++;
            j = 0;
            memset(trueBuf,0,BUFLEN);
            while(buf[i] != ']'){// works only with Time series
                trueBuf[j] = buf[i];
                i++;
                j++;
            }
            i = 0;
                /* get the first token */
            strcpy(buf,trueBuf);
            time_t msec = time(NULL);
            fprintf(fp, "%s - %lld",buf,line);
            fprintf(fp," TS: %ld %d\n",msec,eventMarker);
            ++line;
            key = strtok(buf, token);
            channels[i] = atof(key);
            
            if(debug){
                printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
            }
            i++;
            /* walk through other tokens */
            while( (key = strtok(NULL, token)) != NULL ) {
                
                channels[i] = atof(key);
                if(debug){
                    printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
                }
                i++;
            }
            fprintf(fp2,"1:%f ",channels[0]);
            fprintf(fp2,"2:%f\n",channels[1]);
            
            //pressKey(keyboard,5);
            
        }
        if(timeSeries){
            while(buf[i] != '}')
                i++;
            buf[i+1] = '\0';
            i = 0;
            while(buf[i] != '['){
                i++;
            }
            i++;
            j = 0;
            memset(trueBuf,0,BUFLEN);
            while(buf[i] != ']'){// works only with Time series
                trueBuf[j] = buf[i];
                i++;
                j++;
            }
            i = 0;
                /* get the first token */
            strcpy(buf,trueBuf);
            time_t msec = time(NULL);
            struct tm  ts;
            char       time[80];
            // Format time, "ddd yyyy-mm-dd hh:mm:ss zzz"
            ts = *localtime(&msec);
            strftime(time, sizeof(time), "%a %Y-%m-%d %H:%M:%S", &ts);
            fprintf(fp,"%s - %lld TS: %ld EVENT: %d\n",buf,line,msec,eventMarker);
            ++line;
            if(debug){
                printf("%s - %lld\n" , buf, line);
            }
            key = strtok(buf, token);
            channels[i] = atof(key);
            if(debug){
                printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
            }
            i++;
            /* walk through other tokens */
            while( (key = strtok(NULL, token)) != NULL && i < numberOfChannels) {
                
                channels[i] = atof(key);
                if(debug){
                    printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
                }
                i++;
            }
            int feature = 1;
            if(laplace){
                laplacianFilter(channels);
                laplacianFilters[0] = channels[2];
                laplacianFilters[1] = channels[4];
                laplacianFilters[2] = channels[9];
                laplacianFilters[3] = channels[11];
                laplacianFilters[4] = channels[13];
                for (int a = 0; a < 5; ++a){
                    fprintf(fp2,"%d:%f ",a+1,laplacianFilters[a]);
                }
            }else if(useFilters){
                filterReceivedInputs(channels,filteredInputs);
                printf("%d\n",sizeof(timeLapseToPSD) );
               if(0){
                    /* for (int a = 0; a < numberOfChannels; ++a){
                        if(THETA){
                            timeLapseToPSD[a][0][step-1] = filteredInputs[a][0];
                            
                        }
                        if(DELTA){
                            timeLapseToPSD[a][1][step-1] = filteredInputs[a][1];
                        }
                        if(ALPHA_BAND){
                            timeLapseToPSD[a][2][step-1] = filteredInputs[a][2];
                        }
                        if(BETA){
                            timeLapseToPSD[a][3][step-1] = filteredInputs[a][3];
                        }
                        if(GAMMA){
                            timeLapseToPSD[a][4][step-1] = filteredInputs[a][4];
                        }
                    }
                    ++step;
                    printf("%d\n",step );
                    if(step == 5){
                        for (int a = 0; a < numberOfChannels; ++a){
                            if(THETA){
                                printf("spectrum\n");
                                mtpsd<double> spectrum(timeLapseToPSD[a][0], 5, 100);//This last value i do not get
                                printf("compute\n");
                                spectrum.compute();
                                printf("computed\n");
                                printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                ++feature;
                            }
                            if(DELTA){
                                mtpsd<double> spectrum(timeLapseToPSD[a][1], 5, 2);//This last value i do not get
                                spectrum.compute();
                                printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                ++feature;
                            }
                            if(ALPHA_BAND){
                                mtpsd<double> spectrum(timeLapseToPSD[a][2], 5, 2);//This last value i do not get
                                spectrum.compute();
                                printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                ++feature;
                            }
                            if(BETA){
                                mtpsd<double> spectrum(timeLapseToPSD[a][3], 5, 2);//This last value i do not get
                                spectrum.compute();
                                printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                ++feature;
                            }
                            if(GAMMA){
                                mtpsd<double> spectrum(timeLapseToPSD[a][4], 5, 2);//This last value i do not get
                                spectrum.compute();
                                printf("S=%f, f=%fHz \n", spectrum(0), spectrum.freq(0)); 
                                //fprintf(fp2,"%d:%f ",feature,spectrum(i));
                                ++feature;
                            }
                            

                        }
                        step = 1;
                    }*/
                }else{
                    for (int a = 0; a < numberOfChannels; ++a){
                        if(DELTA){
                            if(chosenChannels[a]){
                                fprintf(fp2,"%d:%f ",feature,filteredInputs[a][0]);
                                ++feature;
                            }
                        }
                        if(THETA){
                            if(chosenChannels[a]){
                                fprintf(fp2,"%d:%f ",feature,filteredInputs[a][1]);
                                ++feature;  
                            }
                        }
                        if(ALPHA_BAND){
                            if(chosenChannels[a]){
                                fprintf(fp2,"%d:%f ",feature,filteredInputs[a][2]);
                                ++feature;
                            }
                        }
                        if(BETA){
                            if(chosenChannels[a]){
                                fprintf(fp2,"%d:%f ",feature,filteredInputs[a][3]);
                                ++feature;
                            }
                        }
                        if(GAMMA){
                            if(chosenChannels[a]){
                                fprintf(fp2,"%d:%f ",feature,filteredInputs[a][4]);
                                ++feature;
                            }
                        }
                    }
                    fprintf(fp2,"\n",eventMarker);
                }
            }else{
                 for (int a = 0; a < numberOfChannels; ++a){
                    if(chosenChannels[a]){
                        fprintf(fp2,"%d:%f ",feature,channels[a]);
                        ++feature;
                    }
                }
                fprintf(fp2,"\n",eventMarker);
            }
        }
        if (powerBand){
            while(buf[i] != '}')
                i++;
            buf[i+1] = '\0';
            i = 0;
            //this is a JSON like structure, lets make it happen
            int channel = 0;
            while(buf[i] != ']' && channel < numberOfChannels){
                int freqBW = 0;
                while(buf[i] != '['){
                    i++;
                }
                //To point to the 1st number
                if(channel == 0)
                    i+=2;
                else
                    i++;
                j=0;
                memset(trueBuf,0,BUFLEN);
                while(buf[i] != ']'){
                    trueBuf[j] = buf[i];
                    i++;
                    j++;
                }
                key = strtok(trueBuf, token);
                if(DELTA && key != NULL){
                    powerBandInputs[channel][0] = atof(key);
                    freqBW++;
                }
                key = strtok(NULL, token);
                if(THETA && key != NULL){
                    powerBandInputs[channel][1] = atof(key);
                    freqBW++;;   
                }
                key = strtok(NULL, token);
                if(ALPHA_BAND && key != NULL){
                    powerBandInputs[channel][2] = atof(key);
                    freqBW++;
                }
                key = strtok(NULL, token);
                if(BETA && key != NULL){
                    powerBandInputs[channel][3] = atof(key);
                    freqBW++;
                }
                key = strtok(NULL, token);
                if(GAMMA && key != NULL){
                    powerBandInputs[channel][4] = atof(key);
                    freqBW++;
                }
                //god help me
               
                channel++;
                i++;
            }
            if(powerDifferenceEnabled){
                powerDifference(powerBandInputs,channel);
                for (int l = 0; l < 15; ++l){
                    for (int k = 0; k < 15*5; ++k){
                        //printf("k %d= %f\n",k, powerBandInputs[l][k]);
                        if(l+1 != channel || k+1 != channel*5){
                            fprintf(fp, "%f,",powerBandInputs[l][k]);
                            if(!record){
                                fprintf(fp2, "%f ",powerBandInputs[l][k]);
                            }
                        }
                        else{
                            fprintf(fp, "%f",powerBandInputs[l][k]);
                            if(!record){
                                fprintf(fp2, "%f ",powerBandInputs[l][k]);
                            }
                        }
                    }
                }
            }else{
                //Get 5 bands
               
                int feature = 1;
                for (int a = 0; a < numberOfChannels; ++a){
                    if(DELTA){
                        if(chosenChannels[a]){
                            fprintf(fp,"%f ",powerBandInputs[a][0]);
                            fprintf(fp2,"%d:%f ",feature,powerBandInputs[a][0]);
                            ++feature;
                        }
                    }
                    if(THETA){
                        if(chosenChannels[a]){
                            fprintf(fp,"%f ",powerBandInputs[a][1]);
                            fprintf(fp2,"%d:%f ",feature,powerBandInputs[a][1]);
                            ++feature;  
                        }
                    }
                    if(ALPHA_BAND){
                        if(chosenChannels[a]){
                            fprintf(fp,"%f ",powerBandInputs[a][2]);
                            fprintf(fp2,"%d:%f ",feature,powerBandInputs[a][2]);
                            ++feature;
                        }
                    }
                    if(BETA){
                        if(chosenChannels[a]){
                            fprintf(fp,"%f ",powerBandInputs[a][3]);
                            fprintf(fp2,"%d:%f ",feature,powerBandInputs[a][3]);
                            ++feature;
                        }
                    }
                    if(GAMMA){
                        if(chosenChannels[a]){
                            fprintf(fp,"%f ",powerBandInputs[a][4]);
                            fprintf(fp2,"%d:%f ",feature,powerBandInputs[a][4]);
                            ++feature;
                        }
                    }
                }
                /*for (int l = 0; l < numberOfChannels; ++l){
                    for (int k = 0; k < bandAmount; ++k){
                        //printf("k %d= %f\n",k, powerBandInputs[l][k]);
                        if(l+1 != channel || k+1 != bandAmount){
                            fprintf(fp, "%d:%f ",feature,powerBandInputs[l][k]);
                            fprintf(fp2, "%d:%f ",feature,powerBandInputs[l][k]);
                            ++feature;
                            
                        }
                        else{
                            fprintf(fp, "%d:%f",feature,powerBandInputs[l][k]);
                            fprintf(fp2, "%d:%f",feature,powerBandInputs[l][k]);
                            ++feature;
                            
                        }
                    }
                }*/
                

            }
            time_t msec = time(NULL);
            fprintf(fp, " %lld TS: %ld %d\n",line,msec,eventMarker);
            fprintf(fp2, "\n",eventMarker);
            
            ++line;
        }
        if(GetAsyncKeyState('L') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            fclose(fp);
            fclose(fp2);
            captureData = FALSE;
        }
        
        if(debug){
            printf("%s\n" , buf);
        }
        //Write the line in the file
        //Test only with meaningful data
        if(debug){
            ftime(&end);
            diff = (int) (1000.0 * (end.time - start.time)
                + (end.millitm - start.millitm));

            printf("\nOperation took %u milliseconds line : %d\n", diff,line);
            ftime(&start);
        }
        
    }

    closesocket(s);
    WSACleanup();
 
    return 0;
}
//Function to connect to the OpenBCI UDP Server
void connectToBCIServer(int port){
    //Initialise winsock
    printf("\nInitialising Winsock...");
    if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
    {
        printf("Failed. Error Code : %d",WSAGetLastError());
        exit(EXIT_FAILURE);
    }
    printf("Initialised.\n");
     
    //create socket
    if ( (s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
    {
        printf("socket() failed with error code : %d" , WSAGetLastError());
        exit(EXIT_FAILURE);
    }
     
    //setup address structure
    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(port);
    si_other.sin_addr.S_un.S_addr = inet_addr(SERVER);
    bind(s, (SOCKADDR*)&si_other, sizeof(si_other));
}
//function to send command to Unity
void sendMessage(const char *msg){
    //Initialise winsock
    sockaddr_in servaddr;
    int fd = socket(AF_INET,SOCK_DGRAM,0);
    if(fd<0){
        perror("cannot open socket");
        return ;
    }
    
    bzero(&servaddr,sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(SERVER);
    servaddr.sin_port = htons(4000);
    if (sendto(fd, msg, strlen(msg)+1, 0, // +1 to include terminator
               (sockaddr*)&servaddr, sizeof(servaddr)) < 0){
        perror("cannot send message");
        closesocket(fd);
        return;
    }
    closesocket(fd);
    return;
}

void powerDifference(double** powerBandInputs,int channel){
    for (int i = 0; i < channel; ++i){
        int pd = 5;
        for (int j = 0; j < channel; ++j){
            if(i != j){
                powerBandInputs[i][pd] = PD(powerBandInputs[i][0],powerBandInputs[j][0]);
                pd++;
                powerBandInputs[i][pd] = PD(powerBandInputs[i][1],powerBandInputs[j][1]);
                pd++;
                powerBandInputs[i][pd] = PD(powerBandInputs[i][2],powerBandInputs[j][2]);
                pd++;
                powerBandInputs[i][pd] = PD(powerBandInputs[i][3],powerBandInputs[j][3]);
                pd++;
                powerBandInputs[i][pd] = PD(powerBandInputs[i][4],powerBandInputs[j][4]);
                pd++;
            }
            
            
        }
    }
    
}

void printUsage(){
    printf("You need to specify the number of channels\n" );
    printf("-c NUM\n");
    printf("You have to use -pb, -ts or -emg also to specify the type of input the client will receive\n");
    printf("You can choose between 3 -mode (capture,svm or testPrint)\n");
}

void predict(struct svm_model *model,double *input,double *output,int finalIndex){

    for (int i = 0; i < finalIndex; ++i){
        x[i].index = i+1;
        x[i].value = input[i];
    }
    x[finalIndex].index = -1;
    *output = svm_predict(model,x);
    
}
void predictEMG(struct svm_model *model,double *input,double *output){

    int svm_type=svm_get_svm_type(model);
    int nr_class=svm_get_nr_class(model);
    double *prob_estimates=NULL;
    for (int i = 0; i < 2; ++i){
        x[i].index = i+1;
        x[i].value = input[i];
    }
    x[2].index = -1;
    *output = svm_predict(model,x);
    
}

void predictTS(struct svm_model *model,double *input,double *output,int finalIndex){

    int svm_type=svm_get_svm_type(model);
    int nr_class=svm_get_nr_class(model);
    double *prob_estimates=NULL;
    for (int i = 0; i < finalIndex; ++i){
        x[i].index = i+1;
        x[i].value = input[i];
    }
    x[finalIndex].index = -1;
    *output = svm_predict(model,x);
    
}

void laplacianFilter(double* channels){
    /*<< 2 might work*/
    channels[2] = (channels[2] *4) - channels[1] - channels[3] - channels[9] - channels[11]; //FC1 = 4*FC1 - F3 - Fz - C3 - Cz
    channels[4] = (channels[4] *4) - channels[3] - channels[5] - channels[11] - channels[13]; //FC2 = 4*FC2 - Fz - F4 - Cz - F4
    channels[9] = (channels[9] *4) - channels[0] - channels[2] - channels[8] - channels[10]; // C3 = 4*C3 - FC5 - FC1 - CP5 - CP1
    channels[11] = (channels[11] *4) - channels[2] - channels[4] - channels[10] - channels[12]; // Cz = 4*Cz - FC1 - FC2 -CP1 - CP2
    channels[13] = (channels[13] *4) - channels[4] - channels[6] - channels[12] - channels[7]; // C4 = 4*C4 - FC2 - FC6 - CP2 - CP6
}

void filterReceivedInputs(double* channels,double** filteredChannels){
    for (int i = 0; i < numberOfChannels; ++i){

        if(THETA){
            filteredChannels[i][0] = bs[i][0].filter(channels[i]);
            filteredChannels[i][0] = lowPass[i][0].filter(filteredChannels[i][0]);
            filteredChannels[i][0] =  highPass[i][0].filter(filteredChannels[i][0]);
            
        }
        if(DELTA){
            filteredChannels[i][1] = bs[i][1].filter(channels[i]);
            filteredChannels[i][1] = lowPass[i][1].filter(filteredChannels[i][1]);
            filteredChannels[i][1] =  highPass[i][1].filter(filteredChannels[i][1]);
            
        }
        if(ALPHA_BAND){
            filteredChannels[i][2] = bs[i][2].filter(channels[i]);

            filteredChannels[i][2] = lowPass[i][2].filter(filteredChannels[i][2]);
            filteredChannels[i][2] =  highPass[i][2].filter(filteredChannels[i][2]);
            
        }
        if(BETA){
            filteredChannels[i][3] = bs[i][3].filter(channels[i]);
            filteredChannels[i][3] = lowPass[i][3].filter(filteredChannels[i][3]);
            filteredChannels[i][3] =  highPass[i][3].filter(filteredChannels[i][3]);
            
        }
        if(GAMMA){
            filteredChannels[i][4] = bs[i][4].filter(channels[i]);
            filteredChannels[i][4] = lowPass[i][4].filter(filteredChannels[i][4]);
            filteredChannels[i][4] =  highPass[i][4].filter(filteredChannels[i][4]);
            
        }
    }

}

void fontSize(int size){
    CONSOLE_FONT_INFOEX cfi = {0};
    cfi.cbSize = sizeof(cfi);
    cfi.nFont = 0;
    cfi.dwFontSize.X = 0;                   // Width of each character in the font
    cfi.dwFontSize.Y = size;                  // Height
    cfi.FontFamily = FF_DONTCARE;
    cfi.FontWeight = FW_NORMAL;
    std::wcscpy(cfi.FaceName, L"Consolas"); // Choose your font
    SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);
    COORD Coord;
    SetConsoleDisplayMode( GetStdHandle( STD_OUTPUT_HANDLE ), 1, &Coord);
}