//**********************************************************
//AUTHOR: Caiã de Aragão Frazão
//The use of this file is to connect the OpenBCI GUI and make some stuff
//BE CAREFUL
//**********************************************************
#include <winsock2.h>
#include <time.h>
#include "svm.h"
#include <iostream>
#include <inttypes.h>
#include <string>
//#pragma comment(lib,"WS2_32.lib") //Winsock Library, is in the folder
 
#define SERVER "127.0.0.1"  //ip address of udp server
#define BUFLEN 4096  //Max length of buffer

typedef int Boolean;
INPUT keyboard[5];
struct sockaddr_in si_other;
WSADATA wsa;
void connectToBCIServer(int port);
void powerDifference(double** powerBandInputs,int channel);
void printUsage();
void predict(struct svm_model *model,double *input,double *output);
void predictEMG(struct svm_model *model,double *input,double *output);
void sendMessage(const char *msg);
void laplacianFilter(double* channels);
#define PD(x,y) ((x-y)/(x+y))
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)
/*
Notes: 
to compile in sublime USE:  "cmd" : ["gcc", "$file_names","-lWS2_32", "-o", "${file_base_name}.exe"]
I'll use a MLP with one hidden layer only to test purposes.

Input: A vector with [N] inputs that are the channels
Output: A vector with [N] binary representations of the pressed key/desired moviment

e.g.:
Input: [-66.071754,3.1962993,3.5315754,-0.17881395,-14.595689,12.762846,-3.486872,-15.310945]
Output: [0,0,0,1] - This represents that 'D' key is being pressed
After some time:
Input: [3.3527615,-15.4897585,-57.891018,56.438152,6.28084,-5.498529,2.8610232,-16.428532]
Output: [0,0,0,0] - This represents key that 'D' was released

*/

#define TRUE 1
#define FALSE 0
/*
To press a key you need to use SendInput(Num of inputs, Pointer to INPUT struct, Sizeof the structure)
UINT WINAPI SendInput(_In_ UINT    nInputs, _In_ LPINPUT pInputs, _In_ int     cbSize);
SHORT WINAPI VkKeyScan(_In_ TCHAR ch);

To release use ki.dwFlags = KEYEVENTF_KEYUP;

*/


