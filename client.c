#include "client.h"
#include "key.h"

#include <sys\timeb.h> 
#define PORT 22348   //The port on which to listen for incoming data
/*
A good disclaimer, this code is mostly a big chunk of lines to not create overhead, I NEED it fast. If you can optimize my mess, fell free and thanks in advance! :)

To build:
g++ -O3 key.c svm.cpp client.c -lWS2_32 -o client.exe
*/
/*
Notes: 
to compile in sublime USE:  "cmd" : ["gcc", "$file_names","-lWS2_32", "-o", "${file_base_name}.exe"]
I'll use a MLP with one hidden layer only to test purposes.

Input: A vector with [N] inputs that are the channels
Output: A vector with [N] binary representations of the pressed key/desired moviment

e.g.:
Input: [-66.071754,3.1962993,3.5315754,-0.17881395,-14.595689,12.762846,-3.486872,-15.310945]
Output: [0,0,0,0.56353] - This represents that 'D' key is being pressed, since it passed the THRESHOLD
After some time:
Input: [3.3527615,-15.4897585,-57.891018,56.438152,6.28084,-5.498529,2.8610232,-16.428532]
Output: [0,0,0,0] - This represents key that 'D' was released

*/
int s;
struct svm_node *x;
int max_nr_attr = 128;
struct svm_model* model;
int main(int argc, char const *argv[]){
	double channels[16]; // Can be max of 16 chan
    int event = 0;
    int port = 22348;
    //This var is a loop control to handle the spaceBar pressing, I DESPERADELY NEED A BETTER WAY
    int cicleControl = 0;
    char eventMarker[33];
    memset(eventMarker,0,33);
    char *key;
    const char *token = ",";
    int slen=sizeof(si_other);
    int numberOfChannels = 0;
    char *buf = new char[BUFLEN];
    char *filename = new char[BUFLEN];
    Boolean captureData = FALSE;
    Boolean testGetInput = FALSE;
    Boolean useSVM = FALSE;
    Boolean powerDifferenceEnabled = FALSE;
    Boolean debug = FALSE;
    Boolean timeSeries = FALSE;
    Boolean emg = FALSE;
    Boolean powerBand = FALSE;
    unsigned long long int line = 1;

    if(argc > 1){
        for (int i = 1; i < argc; ++i){
            if(strstr(argv[i],"-d")){
                debug = TRUE;
            }
            if(strstr(argv[i],"-port")){
                port = atoi(argv[i+1]);
                printf("%d\n",port );
            }
            if(strstr(argv[i],"-mode")){

                if (strstr(argv[i+1],"capture")){
                    
                    captureData = TRUE;
                }else if (strstr(argv[i+1],"svm")){
                    useSVM = TRUE;
                }else if (strstr(argv[i+1],"testInput")){
                    testGetInput = TRUE;
                }
            }
            if(strstr(argv[i],"-pb")){
                powerBand = TRUE;
                if(timeSeries || emg)
                    return -1;
            }
            if(strstr(argv[i],"-ts")){
                timeSeries = TRUE;
                if(powerBand || emg)
                    return -1;
            }
            if(strstr(argv[i],"-emg")){
                emg = TRUE;
                if(powerBand || timeSeries)
                    return -1;
            }
            if(strstr(argv[i],"-c")){
                if(i+1 < argc){
                    numberOfChannels = atoi(argv[i+1]);
                    if(numberOfChannels > 16){
                        printUsage();
                        return -1;
                    }
                }
            }
        }
    }
    if(!numberOfChannels || (!timeSeries && !emg && !powerBand) || (!captureData && !testGetInput && !useSVM)){
        printUsage();
        return -1;
    }
    keyboard[0] = __initKey__();
    keyboard[1] = __initKey__();
    keyboard[2] = __initKey__();
    keyboard[3] = __initKey__();
    keyboard[4] = __initKey__();
    
    double *BCIinputs, *filteredInputs;
    double *KEYoutputs;
    double **powerBandInputs;
    FILE *fp,*fp2;
    if(captureData && powerBand){        
        connectToBCIServer(port);
        fp=fopen("C:\\powerDataSet.txt", "w");
        fp2=fopen("C:\\powerDataSetWithDesiredOutputs.txt", "w");
    } 
    if(captureData && timeSeries){        
        connectToBCIServer(port);
        fp=fopen("C:\\timeDataSet.txt", "w");
        fp2=fopen("C:\\timeDataSetWithDesiredOutputs.txt", "w");
    }
    if(captureData && emg){        
        connectToBCIServer(port);
        fp=fopen("C:\\emgDataSet.txt", "w");
        fp2=fopen("C:\\emgDataSetWithDesiredOutputs.txt", "w");
    } 
    if(emg){
        BCIinputs = (double*) malloc(sizeof(double) * 2);
    }
    if(powerBand){
        BCIinputs = (double*) malloc(sizeof(double) * numberOfChannels*5);
        powerBandInputs =(double**) malloc(sizeof(double*) * numberOfChannels);
        if(powerDifferenceEnabled){
                for (int i = 0; i < numberOfChannels; i++)
                    powerBandInputs[i] = (double*) malloc(sizeof(double) * numberOfChannels*5);
        }else{
            for (int i = 0; i < numberOfChannels; i++)
                powerBandInputs[i] = (double*) malloc(sizeof(double) * 5);
        }
    }
    if(timeSeries){
        BCIinputs = (double*) malloc(sizeof(double) * numberOfChannels);
        *filteredInputs = (double*) malloc(sizeof(double) * 5);
    }
    
    KEYoutputs = (double*) malloc(sizeof(double));
    if(useSVM){
        connectToBCIServer(port);
        int j;
        x = (struct svm_node *) malloc(max_nr_attr*sizeof(struct svm_node));
        if(powerBand || timeSeries){
            if((model=svm_load_model("eeg.data.model"))==0){
                fprintf(stderr,"can't open model file\n");
                exit(1);
            }
        }else{
            if((model=svm_load_model("emg.data.model"))==0){
                fprintf(stderr,"can't open model file\n");
                exit(1);
            }
		}
        
    }
    while(useSVM){
        //Receive message
        //strcpy(buf,"[[67.69558,873.02344,83.17772,151.30247,131.14465],[37.717,699.9358,69.51988,128.18307,113.81166],[39.8543,736.82733,78.77106,134.58551,117.83911],[17.022417,499.88272,38.425323,92.305405,91.67035],[10.0464325,534.57007,38.09386,90.07715,91.36044],[0.804133,127.4574,3.8452852,14.759868,23.321987],[32.947163,650.2743,52.22373,117.00754,105.054306],[93.13989,1060.1652,93.61812,172.0973,141.20903],[410.34283,1503.0592,276.31918,312.72083,201.84933],[23.188759,99.0454,16.767912,17.542273,11.375912],[4.5577106,21.610687,8.495249,6.413058,3.931048],[22.948223,274.36823,69.95364,115.59658,108.44125],[19.22974,268.46805,70.6855,109.552795,94.67353],[11.405738,44.07682,10.522983,9.874128,5.59763],[10.630745,56.007744,16.012032,15.433195,9.970869],[22.391253,266.63406,50.676983,63.0283,51.556255]]}");
        if (recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen) == SOCKET_ERROR){
            printf("recvfrom() failed with error code : %d" , WSAGetLastError());
            exit(EXIT_FAILURE);
        }
        if(debug){
            printf("%s\n",buf );
        }
        //Parses to doubles
        int i = 0;
        int j = 0;
        char trueBuf[BUFLEN];
        if (emg){
             while(buf[i] != '['){
                i++;
            }
            i++;
            j = 0;
            memset(trueBuf,0,BUFLEN);
            while(buf[i] != ']'){// works only with Time series
                trueBuf[j] = buf[i];
                i++;
                j++;
            }
            i = 0;
                /* get the first token */
            strcpy(buf,trueBuf);
            
            ++line;
            
            key = strtok(buf, token);
            channels[i] = atof(key);
            
            if(debug){
                printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
            }
            i++;
            /* walk through other tokens */
            while( (key = strtok(NULL, token)) != NULL ) {
                
                channels[i] = atof(key);
                if(debug){
                    printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
                }
                i++;
            }
            //Threshold implementation
            if(channels[0] > 0.6 && channels[1] > 0.6){
                KEYoutputs[0] = 2;
            }else
                KEYoutputs[0] = 1;
            //SVM implementation
            //BCIinputs[0] = channels[0];
            //BCIinputs[1] = channels[1];
            //predict(model,BCIinputs,KEYoutputs);
            
        }
        if(timeSeries){
            while(buf[i] != '['){
                i++;
            }
            i++;
            memset(trueBuf,0,BUFLEN);
            while(buf[i] != ']'){// works only with Time series
                trueBuf[j] = buf[i];
                i++;
                j++;
            }
            trueBuf[i+1] = '\n';
            trueBuf[i+2] = '\0'; 
            i = 0;
            /* get the first token */
            key = strtok(trueBuf, token);
            BCIinputs[i] = atof(key);
            i++;
            /* walk through other tokens */
            while( (key = strtok(NULL, token)) != NULL ) {
                
                BCIinputs[i] = atof(key);
                //printf("CHANNEL %d: %.6f TOKEN: %s",i,channels[i],token ); // To see channels output
                i++;
            }
            laplacianFilter(BCIinputs);
            laplacianFilter[0] = channels[2];
            laplacianFilter[1] = channels[4];
            laplacianFilter[2] = channels[9];
            laplacianFilter[3] = channels[11];
            laplacianFilter[4] = channels[13];
            predictTS(model,laplacianFilter,KEYoutputs);
        }
        if (powerBand){
            
            //this is a JSON like structure, lets make it happen
            int channel = 0;
            while(buf[i] != ']'){
                int freqBW = 0;
                while(buf[i] != '['){
                    i++;
                }
                //To point to the 1st number
                if(channel == 0)
                    i+=2;
                else
                    i++;
                j=0;
                memset(trueBuf,0,BUFLEN);
                while(buf[i] != ']'){
                    trueBuf[j] = buf[i];
                    i++;
                    j++;
                }
                key = strtok(trueBuf, token);
                powerBandInputs[channel][freqBW] = atof(key);
                if(debug){
                    printf("CHANNEL %d freqBW %d: %.6f TOKEN: %s\n",channel,freqBW,powerBandInputs[channel][freqBW],key ); // To see channels output
                }
                freqBW++;
                /* walk through other tokens */
                while( (key = strtok(NULL, token)) != NULL ) {
                    powerBandInputs[channel][freqBW] = atof(key);
                    if(debug){
                        printf("CHANNEL %d freqBW %d: %.6f TOKEN: %s\n",channel,freqBW,powerBandInputs[channel][freqBW],key ); // To see channels output
                    }
                    freqBW++;
                }
                //god help me
               
                channel++;
                i++;
            }
            //powerDifference(powerBandInputs,channel);
            int m = 0;
           for (int l = 0; l < 16; ++l){
                for (int k = 0; k < 5; ++k){
                    BCIinputs[m] = powerBandInputs[l][k];
                    ++m;
                }
            }
            predict(model,BCIinputs,KEYoutputs);
        }
        printf("OUTPUT: %f\n",KEYoutputs[0]);
        if(KEYoutputs[0] == 1){
           sendMessage("Baseline");
        }
        if(KEYoutputs[0] == 2){
           sendMessage("Hand Movement");
        }
        if(KEYoutputs[0] == 3){
            sendMessage("Hand Imagery");
        }if(KEYoutputs[0] == 4){
            sendMessage("Arm Movement");
        }
        if(KEYoutputs[0] == 5){
            sendMessage("Arm Imagery");
        }
        if(KEYoutputs[0] == 6){
            sendMessage("Third Arm");
        }
        if(GetAsyncKeyState('L') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            
            useSVM = FALSE;
        }
        //Uses the MLP to get the output
        
        /*for (i = 0; i < 5; ++i){
            if (KEYoutputs[i] > 0.7){ //THIS IS A THRESHOLD
                keyboard[i].ki.wVk = keys[i];
                keyboard[i].ki.dwFlags = 0;
            }else{
                keyboard[i].ki.wVk = keys[i];
                keyboard[i].ki.dwFlags = KEYEVENTF_KEYUP;
            }
        }
        //Shows output
        pressKey(keyboard,5);*/
    }
    while(testGetInput){
        if(GetAsyncKeyState('A') & 0x8000/*check if high-order bit is set 1 << 15 (this means 1000000000000000)*/){
            printf("A\n");//1000000000000000
        }
        if(GetAsyncKeyState('S') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            printf("S\n");   
        }
        if(GetAsyncKeyState('W') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            printf("W\n");
        }
        if(GetAsyncKeyState('D') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            printf("D\n");
        }
        if(GetAsyncKeyState('E') & 0x8000/*check if high-order bit is set (1 << 15)*/)
            printf("E\n");
            //return 0;
    }
	//While to capture data to use in the MLP trainer
    int record;
    strcpy(eventMarker,"1 ");// The first action is baseline
    if(captureData){
        printf("Para iniciar a captura\n");
        system("pause");
        system("cls");
    }
        struct timeb start, end;
        ftime(&start);
        
    while(captureData){
        int diff;
        record = FALSE;
        
        //receive a reply and print it
        //clear the buffer by filling null, it might have previously received data
        
        //try to receive some data, this is a blocking call
        //buf = "[[41.669846,49.689392,81.83225,366.5136,329.9111],[0.42361617,0.6239324,20.908672,2.0270483,2.7384207],[0.12620935,0.6780958,0.34697872,2.8156202,244.40503],[0.30882332,0.9020997,0.8557987,3.1498816,2.6902924],[0.34740353,0.5335196,0.885495,3.4016564,2.1252117],[0.2156948,0.4901576,0.38074055,3.1702325,2.386418],[0.158955,0.50894904,0.58843136,1.7801483,3.2499516],[0.25537622,0.54603285,0.3565757,3.4537523,2.4447072]]";
        if (recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen) == SOCKET_ERROR){
            printf("recvfrom() failed with error code : %d" , WSAGetLastError());
            exit(EXIT_FAILURE);
        }
        if(debug){
            printf("%s\n",buf );
        }
        int i = 0;
        int j = 0;
        char trueBuf[BUFLEN];
        char outputBuf[12];
        memset(outputBuf,0,12);
        outputBuf[0] = ' ';
        j++;
        if(GetAsyncKeyState('A') & 0x8000/*check if high-order bit is set 1 << 15 (this means 1000000000000000)*/){
            memset(eventMarker,0,33);
            trueBuf[j] = '1';
            outputBuf[1] = '0';
            strcpy(eventMarker,"1 ");//strcpy(eventMarker,"EVENT: Baseline");
            record = TRUE;
        }else{
            trueBuf[j] = '0';
            outputBuf[1] = '0';
        }
        j++;
        trueBuf[j] = ' ';
        outputBuf[2] = ' ';
        j++;
        if(GetAsyncKeyState('S') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            memset(eventMarker,0,33);
            trueBuf[j] = '1';
            outputBuf[3] = '1';
            strcpy(eventMarker,"2 ");//strcpy(eventMarker,"EVENT:Hand movement");
            record = TRUE;
        }else{
            trueBuf[j] = '0';
            outputBuf[3] = '0';
        }
        j++;
        trueBuf[j] = ' ';
        outputBuf[4] = ' ';
        j++;
        if(GetAsyncKeyState('D') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            memset(eventMarker,0,33);
            trueBuf[j] = '1';
            outputBuf[5] = '1';
            strcpy(eventMarker,"3 ");//strcpy(eventMarker,"EVENT: arm movemente");
            record = TRUE;
        }else{
            trueBuf[j] = '0';
            outputBuf[5] = '0';
        }
        j++;
        trueBuf[j] = ' ';
        outputBuf[6] = ' ';
        j++;
        if(GetAsyncKeyState('F') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            memset(eventMarker,0,33);
            trueBuf[j] = '1';
            outputBuf[7] = '1';
            strcpy(eventMarker,"4 ");//strcpy(eventMarker,"EVENT: arm immagery");
            //record = TRUE;
        }else{
            trueBuf[j] = '0';
            outputBuf[7] = '0';
        }
        j++;
        trueBuf[j] = ' ';
        outputBuf[8] = ' ';
        j++;
        if(GetAsyncKeyState('G') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            trueBuf[j] = '1';
            outputBuf[9] = '1';
            memset(eventMarker,0,33);
            strcpy(eventMarker,"5 ");//strcpy(eventMarker,"EVENT: Immagery, aditional hand");
            record = TRUE;
        }else{
            trueBuf[j] = '0';
            outputBuf[9] = '0';
        }
        if(GetAsyncKeyState('H') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            memset(eventMarker,0,33);
            strcpy(eventMarker,"6 ");//strcpy(eventMarker,"EVENT: Immagery, aditional hand");
            record = TRUE;
        }
        //outputBuf[10] = '\0';
        fprintf(fp2, "%s",eventMarker);
        if(emg){
            while(buf[i] != '['){
                i++;
            }
            i++;
            j = 0;
            memset(trueBuf,0,BUFLEN);
            while(buf[i] != ']'){// works only with Time series
                trueBuf[j] = buf[i];
                i++;
                j++;
            }
            i = 0;
                /* get the first token */
            strcpy(buf,trueBuf);
            time_t msec = time(NULL);
            fprintf(fp, "%s - %lld",buf,line);
            fprintf(fp," TS: %ld %s\n",msec,eventMarker);
            ++line;
            
             key = strtok(buf, token);
            channels[i] = atof(key);
            
            if(debug){
                printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
            }
            i++;
            /* walk through other tokens */
            while( (key = strtok(NULL, token)) != NULL ) {
                
                channels[i] = atof(key);
                if(debug){
                    printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
                }
                i++;
            }
            fprintf(fp2,"1:%f ",channels[0]);
            fprintf(fp2,"2:%f\n",channels[1]);
            
            //pressKey(keyboard,5);
            
        }
        if(timeSeries){
            while(buf[i] != '['){
                i++;
            }
            i++;
            j = 0;
            memset(trueBuf,0,BUFLEN);
            while(buf[i] != ']'){// works only with Time series
                trueBuf[j] = buf[i];
                i++;
                j++;
            }
            i = 0;
                /* get the first token */
            strcpy(buf,trueBuf);
            time_t msec = time(NULL);
            struct tm  ts;
            char       time[80];
            // Format time, "ddd yyyy-mm-dd hh:mm:ss zzz"
            ts = *localtime(&msec);
            strftime(time, sizeof(time), "%a %Y-%m-%d %H:%M:%S", &ts);
            fprintf(fp,"%s - %lld TS: %ld EVENT: %s\n",buf,line,msec,eventMarker);
            ++line;
            if(debug){
                printf("%s - %lld\n" , buf, line);
            }
            key = strtok(buf, token);
            channels[i] = atof(key);
            if(debug){
                printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
            }
            i++;
            /* walk through other tokens */
            while( (key = strtok(NULL, token)) != NULL ) {
                
                channels[i] = atof(key);
                if(debug){
                    printf("CHANNEL %d: %.6f TOKEN: %s\n",i,channels[i],key ); // To see channels output
                }
                i++;
            }
            laplacianFilter(channels);
            laplacianFilter[0] = channels[2];
            laplacianFilter[1] = channels[4];
            laplacianFilter[2] = channels[9];
            laplacianFilter[3] = channels[11];
            laplacianFilter[4] = channels[13];
            for (int a = 0; a < 5; ++a){
                fprintf(fp2,"%d:%f ",a+1,laplacianFilter[a]);
            }
            fprintf(fp2,"\n",eventMarker);
        }
        if (powerBand){
            
            //this is a JSON like structure, lets make it happen
            int channel = 0;
            while(buf[i] != ']'){
                int freqBW = 0;
                while(buf[i] != '['){
                    i++;
                }
                //To point to the 1st number
                if(channel == 0)
                    i+=2;
                else
                    i++;
                j=0;
                memset(trueBuf,0,BUFLEN);
                while(buf[i] != ']'){
                    trueBuf[j] = buf[i];
                    i++;
                    j++;
                }
                key = strtok(trueBuf, token);
                powerBandInputs[channel][freqBW] = atof(key);
                if(debug){
                    printf("CHANNEL %d freqBW %d: %.6f TOKEN: %s\n",channel,freqBW,powerBandInputs[channel][freqBW],key ); // To see channels output
                }
                freqBW++;
                /* walk through other tokens */
                while( (key = strtok(NULL, token)) != NULL ) {
                    powerBandInputs[channel][freqBW] = atof(key);
                    if(debug){
                        printf("CHANNEL %d freqBW %d: %.6f TOKEN: %s\n",channel,freqBW,powerBandInputs[channel][freqBW],key ); // To see channels output
                    }
                    freqBW++;
                }
                //god help me
               
                channel++;
                i++;
            }
            if(powerDifferenceEnabled){
                powerDifference(powerBandInputs,channel);
                for (int l = 0; l < 15; ++l){
                    for (int k = 0; k < 15*5; ++k){
                        //printf("k %d= %f\n",k, powerBandInputs[l][k]);
                        if(l+1 != channel || k+1 != channel*5){
                            fprintf(fp, "%f,",powerBandInputs[l][k]);
                            if(!record){
                                fprintf(fp2, "%f ",powerBandInputs[l][k]);
                            }
                        }
                        else{
                            fprintf(fp, "%f",powerBandInputs[l][k]);
                            if(!record){
                                fprintf(fp2, "%f ",powerBandInputs[l][k]);
                            }
                        }
                    }
                }
            }else{
                //Get 5 bands
                int feature = 1;
                for (int l = 0; l < 16; ++l){
                    for (int k = 0; k < 5; ++k){
                        //printf("k %d= %f\n",k, powerBandInputs[l][k]);
                        if(l+1 != channel || k+1 != 5){
                            fprintf(fp, "%d:%f ",feature,powerBandInputs[l][k]);
                            fprintf(fp2, "%d:%f ",feature,powerBandInputs[l][k]);
                            ++feature;
                            
                        }
                        else{
                            fprintf(fp, "%d:%f",feature,powerBandInputs[l][k]);
                            fprintf(fp2, "%d:%f",feature,powerBandInputs[l][k]);
                            ++feature;
                            
                        }
                    }
                }
                

            }
            time_t msec = time(NULL);
            fprintf(fp, " %d TS: %lld %s\n",line,msec,eventMarker);
            fprintf(fp2, "\n",line,msec,eventMarker);
            
            ++line;
        }
        trueBuf[j] = ',';
        
        //fwrite(buf, sizeof(buf), sizeof(buf)/sizeof(buf[i]), fp); //binary
        
        trueBuf[j+1] = '\n';
        trueBuf[j+2] = '\0';
        if(GetAsyncKeyState('L') & 0x8000/*check if high-order bit is set (1 << 15)*/){
            fclose(fp);
            fclose(fp2);
            captureData = FALSE;
        }
        
        if(debug){
            printf("%s\n" , buf);
        }
        //Write the line in the file
        //Test only with meaningful data
        if(1){
            ftime(&end);
            diff = (int) (1000.0 * (end.time - start.time)
                + (end.millitm - start.millitm));

            printf("\nOperation took %u milliseconds line : %d\n", diff,line);
            ftime(&start);
        }
        
    }
    if(useSVM){
        
    }

    closesocket(s);
    WSACleanup();
 
    return 0;
}
//Function to connect to the OpenBCI UDP Server
void connectToBCIServer(int port){
    //Initialise winsock
    printf("\nInitialising Winsock...");
    if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
    {
        printf("Failed. Error Code : %d",WSAGetLastError());
        exit(EXIT_FAILURE);
    }
    printf("Initialised.\n");
     
    //create socket
    if ( (s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
    {
        printf("socket() failed with error code : %d" , WSAGetLastError());
        exit(EXIT_FAILURE);
    }
     
    //setup address structure
    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(port);
    si_other.sin_addr.S_un.S_addr = inet_addr(SERVER);
    bind(s, (SOCKADDR*)&si_other, sizeof(si_other));
}
//function to send command to Unity
void sendMessage(const char *msg){
    //Initialise winsock
    sockaddr_in servaddr;
    int fd = socket(AF_INET,SOCK_DGRAM,0);
    if(fd<0){
        perror("cannot open socket");
        return ;
    }
    
    bzero(&servaddr,sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(SERVER);
    servaddr.sin_port = htons(4000);
    if (sendto(fd, msg, strlen(msg)+1, 0, // +1 to include terminator
               (sockaddr*)&servaddr, sizeof(servaddr)) < 0){
        perror("cannot send message");
        closesocket(fd);
        return;
    }
    closesocket(fd);
    return;
}

void powerDifference(double** powerBandInputs,int channel){
    for (int i = 0; i < channel; ++i){
        int pd = 5;
        for (int j = 0; j < channel; ++j){
            if(i != j){
                powerBandInputs[i][pd] = PD(powerBandInputs[i][0],powerBandInputs[j][0]);
                pd++;
                powerBandInputs[i][pd] = PD(powerBandInputs[i][1],powerBandInputs[j][1]);
                pd++;
                powerBandInputs[i][pd] = PD(powerBandInputs[i][2],powerBandInputs[j][2]);
                pd++;
                powerBandInputs[i][pd] = PD(powerBandInputs[i][3],powerBandInputs[j][3]);
                pd++;
                powerBandInputs[i][pd] = PD(powerBandInputs[i][4],powerBandInputs[j][4]);
                pd++;
            }
            
            
        }
    }
    
}

void printUsage(){
    printf("You need to specify the number of channels\n" );
    printf("-c NUM\n");
    printf("You have to use -pb, -ts or -emg also to specify the type of input the client will receive\n");
    printf("You can choose between 3 -mode (capture,svm or testInput)\n");
}

void predict(struct svm_model *model,double *input,double *output){

    int svm_type=svm_get_svm_type(model);
    int nr_class=svm_get_nr_class(model);
    double *prob_estimates=NULL;
    for (int i = 0; i < 80; ++i){
        x[i].index = i+1;
        x[i].value = input[i];
    }
    x[80].index = -1;
    *output = svm_predict(model,x);
    
}
void predictEMG(struct svm_model *model,double *input,double *output){

    int svm_type=svm_get_svm_type(model);
    int nr_class=svm_get_nr_class(model);
    double *prob_estimates=NULL;
    for (int i = 0; i < 2; ++i){
        x[i].index = i+1;
        x[i].value = input[i];
    }
    x[2].index = -1;
    *output = svm_predict(model,x);
    
}

void predictTS(struct svm_model *model,double *input,double *output){

    int svm_type=svm_get_svm_type(model);
    int nr_class=svm_get_nr_class(model);
    double *prob_estimates=NULL;
    for (int i = 0; i < 5; ++i){
        x[i].index = i+1;
        x[i].value = input[i];
    }
    x[5].index = -1;
    *output = svm_predict(model,x);
    
}

void laplacianFilter(double* channels){
    /*<< 2 might work*/
    channels[2] = (channels[2] *4) - channels[1] - channels[3] - channels[9] - channels[11]; //FC1 = 4*FC1 - F3 - Fz - C3 - Cz
    channels[4] = (channels[2] *4) - channels[3] - channels[5] - channels[11] - channels[13]; //FC2 = 4*FC2 - Fz - F4 - Cz - F4
    channels[9] = (channels[9] *4) - channels[0] - channels[2] - channels[8] - channels[10]; // C3 = 4*C3 - FC5 - FC1 - CP5 - CP1
    channels[11] = (channels[11] *4) - channels[2] - channels[4] - channels[10] - channels[12]; // Cz = 4*Cz - FC1 - FC2 -CP1 - CP2
    channels[13] = (channels[13] *4) - channels[4] - channels[6] - channels[12] - channels[7]; // C4 = 4*C4 - FC2 - FC6 - CP2 - CP6
}