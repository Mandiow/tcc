

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Text;

public class ControlScript : MonoBehaviour {
	
	
	//First, we will create a reference called myAnimator so we can talk to the Animator component on the game object.
	//The Animator is what listens to our instructions and tells the mesh which animation to use.
	private Animator myAnimator;
    public GameObject target;
    public int thumb;
    public int index;
    public int middle;
    public int ring;
    public int little;
    public GameObject collar;
    public Transform trans;
    string returnData = "";
    public int hand;
    static UdpClient udp;
    Thread thread;
    // The start method is called when the script is initalized, before other stuff in the scripts start happening.
    void Start () {
		//We have a reference called myAnimator but we need to fill that reference with an Animator component.
		//We can do that by 'getting' the animator that is on the same game object this script is appleid to.
		myAnimator = GetComponent<Animator>();
        UnityEngine.XR.InputTracking.Recenter();
        udp = new UdpClient(4000);
        thread = new Thread(new ThreadStart(ThreadMethod));
        thread.Start();
        //SkinnedMeshRenderer targetRenderer = target.GetComponent<SkinnedMeshRenderer>();
        //Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();
        

    }
	
	// Update is called once per frame so this is a great place to listen for input from the player to see if they have
	//interacted with the game since the LAST frame (such as a button press or mouse click).
	void FixedUpdate () {

        // This is a very dumb way to make a decay to make sure every single movement is independent, could not think in nothing better atm
        if (returnData.Contains("Baseline")) {
            myAnimator.SetBool("KeyPressed", false);
            myAnimator.SetBool("ArmPressed", false);
            myAnimator.SetBool("right", false);
        } else if (returnData.Contains("Hand Movement")) {
            myAnimator.SetBool("KeyPressed", true);
            myAnimator.SetBool("right", false);
            myAnimator.SetBool("ArmPressed", false);
        } else if (returnData.Contains("Hand Imagery")) {
            myAnimator.SetBool("KeyPressed", true);
            myAnimator.SetBool("right", false);
            myAnimator.SetBool("ArmPressed", false);
        } else if (returnData.Contains("Arm Movement")) {
            myAnimator.SetBool("ArmPressed", true);
            myAnimator.SetBool("right", false);
            myAnimator.SetBool("KeyPressed", false);

        } else if (returnData.Contains("Arm Imagery")) {
            myAnimator.SetBool("ArmPressed", true);
            myAnimator.SetBool("right", false);
            myAnimator.SetBool("KeyPressed", false);

        } else if (returnData.Contains("Third Arm")) {
            myAnimator.SetBool("right", true);
            myAnimator.SetBool("KeyPressed", false);
            myAnimator.SetBool("KeyPressed", false);

        }
    }

    private void ThreadMethod(){
        while (true) {
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Loopback, 0);

            byte[] receiveBytes = udp.Receive(ref RemoteIpEndPoint);
            returnData = Encoding.ASCII.GetString(receiveBytes);
            
                

        }
    }
    void OnApplicationQuit(){
        udp.Close();
        thread.Abort();
    }
        
        void OnGUI(){
		
	}
}
